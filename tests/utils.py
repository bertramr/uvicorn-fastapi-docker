import json
import os
from typing import Any, Dict, List

from docker.client import DockerClient
from docker.errors import NotFound
from docker.models.containers import Container

CONTAINER_NAME = "uvicorn-fastapi-docker-test"


def get_process_names(container: Container, _filter: str) -> List[str]:
    top = container.top()
    process_commands = [p[7] for p in top["Processes"]]
    processes = [p for p in process_commands if _filter in p]
    return processes


def remove_previous_container(client: DockerClient) -> None:
    try:
        previous = client.containers.get(CONTAINER_NAME)
        previous.stop()
        previous.remove()
    except NotFound:
        return None


def get_logs(container: DockerClient) -> str:
    logs = container.logs()
    return logs.decode("utf-8")


def get_response_text1() -> str:
    python_version = os.getenv("PYTHON_VERSION")
    return f"Hello world! From FastAPI running on Uvicorn. Using Python 3.9"
