import os
import time

import docker
import requests
from docker.client import DockerClient

from ..utils import (
    CONTAINER_NAME,
    get_process_names,
    get_logs,
    get_response_text1,
    remove_previous_container,
)

client = docker.from_env()


def verify_container(container: DockerClient, response_text: str) -> None:
    response = requests.get("http://127.0.0.1:8000")
    data = response.json()
    assert data["message"] == response_text
    uvicorn_process = get_process_names(container, "uvicorn")
    assert "/usr/local/bin/python /usr/local/bin/uvicorn --host 0.0.0.0 --port 80 --log-level info main:app" == uvicorn_process[0]
    logs = get_logs(container)
    assert "Checking for script in /app/prestart.sh" in logs
    assert "Running script /app/prestart.sh" in logs
    assert (
        "Running inside /app/prestart.sh, you could add migrations to this file" in logs
    )
    assert '"GET / HTTP/1.1" 200' in logs
    assert "Application startup complete." in logs
    assert "Uvicorn running on http://0.0.0.0:80" in logs


def test_defaults() -> None:
    name = os.getenv("NAME", "latest")
    image=f"registry.gitlab.com/bertramr/uvicorn-fastapi-docker:{name}"
    client.login(
        username=os.getenv("CI_REGISTRY_USER"),
        password=os.getenv("CI_REGISTRY_PASSWORD"),
        registry=os.getenv("CI_REGISTRY")
    )

    response_text = get_response_text1()
    sleep_time = int(os.getenv("SLEEP_TIME", 1))
    remove_previous_container(client)
    container = client.containers.run(
        image, name=CONTAINER_NAME, ports={"80": "8000"}, detach=True
    )
    time.sleep(sleep_time)
    verify_container(container, response_text)
    container.stop()
    # Test that everything works after restarting too
    container.start()
    time.sleep(sleep_time)
    verify_container(container, response_text)
    container.stop()
    container.remove()
