import os
import subprocess
import requests
from subprocess import PIPE
from ..utils import get_response_text1
import time

compose_file = os.path.join(os.path.dirname(__file__), "docker-compose.yml")

def test_loadbalancer() -> None:
	command = ['docker-compose', "-f", compose_file, 'up', '-d']
	process = subprocess.run(command, stdout=PIPE, stderr=PIPE)

	time.sleep(2)

	for i in range(10):
		response = requests.get("http://127.0.0.1:8000")
	data = response.json()
	assert data["message"] == get_response_text1()
	
	command = ['docker-compose', "-f", compose_file, 'logs', "web"]
	process = subprocess.run(command, stdout=PIPE)

	time.sleep(1)
	print(process.stdout.decode())
	command = ['docker-compose', "-f", compose_file, 'down', "-v", "--remove-orphans"]
	process = subprocess.run(command, stdout=PIPE, stderr=PIPE)

	#print(process.stdout.decode() + process.stderr.decode(), process.returncode)

	assert 0 == 1