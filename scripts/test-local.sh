#!/usr/bin/env bash
set -ev

docker pull "registry.gitlab.com/bertramr/uvicorn-fastapi-docker:latest"
pip install docker[tls] pytest

pytest tests
