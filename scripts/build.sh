#!/usr/bin/env bash
set -ev

use_tag="registry.gitlab.com/bertramr/uvicorn-fastapi-docker:$NAME"

DOCKERFILE="$NAME"

if [ "$NAME" == "latest" ]; then
  DOCKERFILE="python3.9-slim"
fi

export DOCKER_BUILDKIT=1
docker pull --quiet "$use_tag" || true
docker build --tag "$use_tag" \
  --cache-from "$use_tag" \
  --quiet \
  --build-arg BUILDKIT_INLINE_CACHE=1 \
  --file "./docker-images/${DOCKERFILE}.dockerfile" \
  "./docker-images/"
