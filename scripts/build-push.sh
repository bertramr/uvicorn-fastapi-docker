#!/usr/bin/env bash

set -ev

use_tag="registry.gitlab.com/bertramr/uvicorn-fastapi-docker:$NAME"
use_dated_tag="${use_tag}-$(date -I)"

bash scripts/build.sh

docker tag "$use_tag" "$use_dated_tag"

docker push --quiet "$use_tag"
docker push --quiet "$use_dated_tag"
